"use strict";
const mongoose = require("mongoose");
const Post = mongoose.model("Post");

exports.get = (req, res, next) => {
    let filter = new String(req.params.type).toUpperCase();
    console.log(filter);
    
    let slugPage = new String(req.params.slug).toUpperCase();

    filter = (filter == 'PAGE' || filter == 'POST')
        ? { type: filter }
        : {};
    

    Post.find(filter, 'title slug type altered')
        .then( posts => {
            res.status(200).send(posts);
        } )
        .catch(err => {
            res.status(400)
                .send(`ocorreu um erro ao listar\n ${err}`)
        })
}

exports.getOne = (req, res, next) => {
    let typer = new String(req.params.type);
    let filter = new String(req.params.slug);
    const query = { };
    if(typer != undefined) query.type = typer;
    if(filter != undefined) query.slug = filter;
    Post
        .findOne(query, 'title slug type')
        .then(post => {
            res.status(200)
                .send(post);
        }).catch(err => {
            res.status(400)
                .send(err);
        })
};

exports.getCategory = (req, res, next) => {
    const category = new String(req.params.cat) || undefined;
    const query = category == undefined ? { } : {category: category};
    Post.find(query)
        .then(post =>
            res.status(200)
                .send(post))
        .catch(err =>
            res.status(400)
                .send(er));
};

exports.post = (req, res, next) => {
    const {title, slug, content, category, created, altered, authorId, type} = req.body;
    const post = new Post({title, slug, content, category, created, altered, authorId, type});
    post.save()
        .then( x => {
            res.status(200).send({
                message: "Postagem inserida com sucesso",
                item: req.body
            });
        })
        .catch( err => {
            res.status(400)
                .send({
                    message: "Postagem não pôde ser inserida",
                    data: err
                })
        })
    ;
}

exports.put = (req, res, next) => {
    const id = req.params.id;
    Post
        .findByIdAndUpdate(id,{
            $set: {
                title: req.body.title,
                content: req.body.content,
                altered: new Date(),
                image: req.body.image
            }
        }).then(post => {
            res.status(200)
                .send({
                    message: "Post altere with sucess"
                });
        }).catch(err => {
            res.status(400).send(err)
        })
    // res.status(200).send({
    //     id: id,
    //     item: req.body
    // })
}

exports.delete = (req, res, next) => {
    const id = req.params.id;
    res.status(200).send({
        id: id,
        item: req.body
    })
};