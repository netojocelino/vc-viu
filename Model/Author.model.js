const mongoose = require("mongoose");

const AuthorModel = {
    name: {
        type: String,
        require: true,
        unique: true,
        uppercase: true
    },
    username: {
        type: String,
        require: true,
        unique: true,
        uppercase: true
    },
    password: {
        type: String,
        require: true,
        unique: true,
        uppercase: true
    },
    image: {
        type: String
    },
    role: {
        type: String,
        require: true,
        default: "guest",
        enum: ["author", "admin", "guest"]
    }
};


const AuthorSchema = mongoose.Schema(AuthorModel);

module.exports = mongoose.model("Author", AuthorSchema);