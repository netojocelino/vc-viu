const mongoose = require("mongoose");

const PostModel = {
    title: {
        type: String,
        require: true,
        trim: true
    },
    slug: {
        type: String,
        require: true,
        trim: true,
        unique: true,
        index: true
    },
    content: {
        type: String,
        require: true
    },
    category: [{
        type: String,
        require: true
    }],
    created: {
        type: Date,
        require: true,
        default: new Date()
    },
    altered: {
        type: Date,
        require: false,
        default: new Date()
    },
    authorId: {
        type: Number,
        require: true
    },
    type:{
        type: String,
        uppercase: true,
        require: true,
        enum: ["POST", "PAGE"]
    },
    image: {
        type: String
    }
};

const PostSchema = mongoose.Schema(PostModel);

module.exports = mongoose.model("Post", PostSchema);