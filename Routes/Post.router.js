const Post = require("../Model/Post.model");

const postRouter = (express) => {
    const router = express.Router();
    const controller = require("../Controllers/Post.controller");


    router.get("/", controller.get);
    router.get("/:type", controller.get);
    router.get("/:type/:slug", controller.getOne);
    router.get("/:type/category", controller.getCategory);

    router.post("/new", controller.post);

    router.put("/edit/:id", controller.put);

    router.delete("/delete/:id", controller.delete);
    
    return router;
};

module.exports = postRouter;