const express = require("express");
const bodyParser = require("body-parser");


const mongoose = require("mongoose");
// mongoose.connect("mongodb://localhost:7521/vcviu");
mongoose.connect("mongodb://root:123Root@ds044587.mlab.com:44587/post");


const app = express();

const Post = require("./Model/Post.model");



const postRouter = require("./Routes/Post.router")(express);


app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

app.get("/", function(req, res){
    res.send({
        header: {
            name: "API home page",
            author: "netojocelino"
        },
        status: 200
    });
});

app.use("/blog", postRouter);


module.exports = app;