const nodemon = require("nodemon");

nodemon({
    script: "./server.js",
    ext: "js json"
});


nodemon.on("start", () => {
  console.log("Welcome to nodemon");
    
}).on("quit", () => {
    console.log("See u later");
    process.exit();
}).on("restart", (files) => {
    console.log(files,"was modified");
    
})